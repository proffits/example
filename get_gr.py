#!/usr/bin/env python

import time
start_time = time.time()
from resource import getrusage, RUSAGE_SELF

import numpy as np
import matplotlib.pyplot as plt

from proffits import fitDr, Gudrun_extra
from proffits.extraScattering import ZeroFunc, Linear, Quadratic
from proffits.fileio import inst, Gudrun, merge_params

#################################
# Useful parameters

# Data file with the Gudrun output
dcsfile = 'Gudrun processing/SiC_GEM57664.dcs01'

rmax = 100  # Maximum value of r
nr = 4000  # number of r points

# Where to save the output
npzfile = 'Si_C.npz'
grfile = 'Si_C.dr'

#################################

def plotDCS(dcs, basename):
    fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, sharex=False)

    ax1.set_xlim(0, 5)
    ax1.set_ylim(-1, 4)
    ax2.set_ylim(0, 2)

    NaN_for_no_data = dcs.data[:, 2::2] / dcs.data[:, 2::2]
    ax1.plot(dcs.q, dcs.data[:, 1::2] * NaN_for_no_data)
    ax2.plot(dcs.q, dcs.data[:, 1::2] * NaN_for_no_data)

    fig.savefig(basename + '_DCS.pdf')


def plotDCSbanks(dcs, basename):
    fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, sharex=False)

    ax1.set_xlim(0, 5)
    ax1.set_ylim(-5, 20)
    ax2.set_ylim(-5, 15)

    for bank in dcs.banks[1:]:
        ax1.plot(bank.q, bank.qsexpt)
        ax2.plot(bank.q, bank.qsexpt, lw=0.5)

    fig.savefig(basename + '_QSQ.pdf')

# Gudrun output:
# Gudrun_dcs> Average level of merged dcs is    0.36076 b/sr/atom;
print("Loading Gudrun output from '%s'..." % dcsfile)
dcs = Gudrun_extra.DCS_extra(dcsfile, sumcoeffs=0.36076)

plotDCS(dcs, 'Si_C_gud')
plotDCSbanks(dcs, 'Si_C_gud')

print("Subtracting small-angle scattering...")
A, power = dcs.subtractSmallAngleScattering(fit_range=(0.1, 1.1))
print("... Fitted power law SANS = %f * q**%f" % (A, power))

plotDCS(dcs, 'Si_C_noSANS')
plotDCSbanks(dcs, 'Si_C_noSANS')


print("Reading instrument parameters...")
# TODO generate a .inst file from the data Matt sent me
inst_params = inst.Inst2File('instrument parameters/GEM56239.inst')
groups = Gudrun.GudrunGroupFile('Gudrun processing/gudrun_grp.dat')

pairs = merge_params.merge_grp_with_inst(groups, inst_params)

print("R array: rmax = %f, nr = %d" % (rmax, nr))
r = np.linspace(rmax, 0, nr, endpoint=False)[::-1]

print("Calculating S(Q) data constraints...")
dataconstraints = []
for inst_grp in pairs:
    igrp = inst_grp.igroup
    print("... Gudrun group %d, inst file block %d, twotheta = %f, npts = %d" %
	  (igrp, inst_grp.bank_num, 2 * np.rad2deg(inst_grp.theta), len(dcs.banks[igrp].q)))
    dataconstraints.append(fitDr.SQDataConstraint(r, dcs.banks[igrp], inst_grp))

print("Calculating additional constraints...")
rmin = 1.1
print("... Minimum R: %f" % rmin)
rmin_c = fitDr.MinimumRConstraint(r, rmin=rmin, weight=1e4)
qmin = 1.1
dqmin = 0.001
print("... Minimum Q: %f, dq = %f" % (qmin, dqmin))
qmin_c = fitDr.MinimumQConstraint(r, dq=dqmin, qmin=qmin, weight=1e2)
print("... Smoothness constraint")
smooth = fitDr.SmoothnessConstraint(r, weight=1e-6)

print("Building the linear least squares model...")
print("... including fitted quadratics for each QS(Q)")
model, npoly = fitDr.buildModel(dataconstraints + [rmin_c, qmin_c, smooth],
				fit_polynomial=[0, 1, 2])

print("Running the fit calculation...")
fit_result = model.fit()
gr = fit_result.params[:nr]
poly_params = fit_result.params[nr:]
gr_errs = fit_result.bse[:nr]
poly_errs = fit_result.bse[nr:]

rusage = getrusage(RUSAGE_SELF)

print("User CPU time used:", rusage.ru_utime)
print("System CPU time used:", rusage.ru_stime)
print("Total CPU time used:", time.clock())
print("Elapsed (wall clock) time:", time.time() - start_time)
print("Memory used: %d kB" % rusage.ru_maxrss)

print("Saving results to '%s'..." % npzfile)
results_dict = dict()
for idata, inst_grp in enumerate(pairs):
    ibank = inst_grp.bank_num
    igrp = inst_grp.igroup
    results_dict["bank%d_q" % ibank] = dcs.banks[igrp].q
    results_dict["bank%d_sexpt" % ibank] = dcs.banks[igrp].qsexpt
    results_dict["bank%d_scalc" % ibank] = dataconstraints[idata].transform(gr)
    results_dict["bank%d_chi2" % ibank] = dataconstraints[idata].chi2(gr)
    q = dcs.banks[igrp].q
    c, b, a = poly_params[3*idata:3*(idata+1)]
    results_dict["bank%d_polynomial" % ibank] = a * q**2 + b * q + c

# Add resource usage details to the results dict
for key in rusage.__dir__():
    if key.startswith('ru_'):
        results_dict[key] = rusage.__getattribute__(key)

np.savez(npzfile, r=r, gr=gr, gr_errs=gr_errs,
	 poly_params=poly_params, poly_errs=poly_errs,
	 dcs_data=dcs.data,
	 cpu_time=time.clock(),
	 wallclock_time=(time.time() - start_time),
	 **results_dict)

print("Saving D(r) data to '%s'..." % grfile)
np.savetxt(grfile, np.column_stack((r, gr, gr_errs)),  # Columns are r, D(r), standard error
	   comments='', header="%d\n%s" % (nr,  # Write the number of data points
				           dcs.description))  # One header line - use the title from the .dcs01 file
