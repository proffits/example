#!/bin/sh

# Set up git annex and get the data files from FigShare
git annex init
git annex get

# Check out the ProfFitS code from gitorious
git submodule init
git submodule update

# Run gudrun to generate the .dcs01 data file
make -C "Gudrun processing"

# Compile the optimised fortran ToF profile code
make -C proffits/tofprofiles
