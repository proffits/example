# Example calculation with ProfFitS #

This repository is an example of a data repository set up to calculate a G(r) using ProfFitS

## Usage ##

1. Run `sh setup.sh`

2. Run `python get_gr.py`

## Requirements ##

* git-annex [http://git-annex.branchable.com/](http://git-annex.branchable.com/)

* gfortran with OpenMP support

* A working f2py (part of [numpy](http://www.numpy.org) )

* Gudrun (get it from [http://disordmat.moonfruit.com/](http://disordmat.moonfruit.com/) ), installed in /opt/Gudrun4
